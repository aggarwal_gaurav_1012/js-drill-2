function findWebDevelopers(inventory) {
    const webDevelopers = [];
    for (let i = 0; i < inventory.length; i++) {
        const person = inventory[i];

        // ind all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
        if (person.job.includes("Web Developer")) {
            webDevelopers.push(person);
        }
    }
    return webDevelopers;
}
module.exports = findWebDevelopers;
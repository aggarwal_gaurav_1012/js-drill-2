function averageSalaryByCountry(inventory) {
    const countrySalaries = {};

    // Loop through the inventory
    for (let i = 0; i < inventory.length; i++) {
        const employee = inventory[i];
        const country = employee.location;
        const salary = parseFloat(employee.salary.replace('$', ''));

        // If the country entry doesn't exist, create it
        if (!countrySalaries[country]) {
            countrySalaries[country] = { totalSalary: 0, count: 0 };
        }

        // Update the total salary and count for the country
        countrySalaries[country].totalSalary += salary;
        countrySalaries[country].count++;
    }

    // Calculate average salary for each country
    for (const country in countrySalaries) {
        const average = countrySalaries[country].totalSalary / countrySalaries[country].count;
        countrySalaries[country].averageSalary = average.toFixed(2);
        delete countrySalaries[country].totalSalary;
        delete countrySalaries[country].count;
    }

    return countrySalaries;
}
module.exports = averageSalaryByCountry;
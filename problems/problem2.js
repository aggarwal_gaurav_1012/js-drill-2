function convertSalaryToNumber(inventory) {
    for (let i = 0; i < inventory.length; i++) {

        // Convert all the salary values into proper numbers instead of strings.
        inventory[i].salary = parseFloat(inventory[i].salary.replace('$', ''));
    }
    return inventory;
}
module.exports = convertSalaryToNumber;
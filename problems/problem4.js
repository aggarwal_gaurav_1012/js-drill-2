function sumSalaries(inventory) {
  let totalSalary = 0;
  for (let i = 0; i < inventory.length; i++) {
    const salaryStr = inventory[i].salary.replace('$', '');
    const salary = parseFloat(salaryStr);

    // To calculate total salary
    totalSalary += salary;
  }
  return totalSalary;
}
module.exports = sumSalaries;  
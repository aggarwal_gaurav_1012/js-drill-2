function correctSalaries(inventory) {
    for (let i = 0; i < inventory.length; i++) {
        const salaryString = inventory[i].salary.replace('$', '');
        const salary = parseFloat(salaryString) * 10000;
        inventory[i].corrected_salary = salary;
    }
    return inventory;
}
module.exports = correctSalaries;
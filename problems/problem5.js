function sumSalariesByCountry(inventory) {
    const salaryByCountry = {};

    for (let i = 0; i < inventory.length; i++) {
        const employee = inventory[i];
        const country = employee.location;

        if (!salaryByCountry[country]) {
            salaryByCountry[country] = 0;
        }

        // Extracting salary without $ sign and converting it to a float
        const salary = parseFloat(employee.salary.replace('$', ''));
        salaryByCountry[country] += salary;
    }
    return salaryByCountry;
}
module.exports = sumSalariesByCountry;